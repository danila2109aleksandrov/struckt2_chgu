﻿// struckt2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#pragma warning(disable : 4996)

using namespace std;

struct TIME
{
    int hours;
    int minutes;
    int seconds;
    void time()
    {
        struct TIME t;
        // Ввод часов
        printf("Enter hour: ");
        scanf("%i", &t.hours);
        // Ввод минут
        printf("Enter minute: ");
        scanf("%i", &t.minutes);
        // Ввод секунд
        printf("Enter second: ");
        scanf("%i", &t.seconds);
        t.seconds++;
        if (t.seconds > 60)
        {
            t.minutes++;
            t.seconds = 0;
        }
        if (t.minutes > 60)
        {
            t.hours++;
            t.minutes = 0;
        }
        if (t.hours > 24)
        {
            t.hours = 0;
        }
        // Вывод времени
        printf("Time  %i:%i:%i", t.hours, t.minutes, t.seconds);

    }

};



int main()
{
    TIME t;
    t.time();
    return 0;
}


